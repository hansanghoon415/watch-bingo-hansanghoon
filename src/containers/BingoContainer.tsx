import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { BingoAppState } from 'modules';
import { actionCreators as gameActions } from 'modules/game';

import { BingoTable, CompletedBingoList, UserId } from 'components';
import { Bingo as BingoClass } from 'models';

interface IProps {
  id: string;
  bingo: BingoClass;
  gameStatus: string;
  GameActions: typeof gameActions;
}

const mapStateToProps = (state: BingoAppState, ownProps: any) => ({
  bingo: state.game.bingos.getById(ownProps.id),
  gameStatus: state.game.gameStatus,
});

const mapDispatchToProps = (dispatch: any) => ({
  GameActions: bindActionCreators(gameActions, dispatch),
});

const Bingo: React.FC<IProps> = (props) => {
  const { GameActions, gameStatus, bingo } = props;
  const checkNumber = (value: number) => {
    GameActions.checkNumber({ userId: bingo.userId, value });
  };

  return (
    <div style={{ display: 'flex' }}>
      <UserId userId={`${bingo.userId + 1}`} />
      <BingoTable
        data={bingo.cells}
        checkNumber={checkNumber}
        gameStatus={gameStatus}
        tableSize={bingo.tableSize}
      />
      <CompletedBingoList data={bingo.completedLines} />
    </div>
  );
};

export const BingoContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Bingo);
