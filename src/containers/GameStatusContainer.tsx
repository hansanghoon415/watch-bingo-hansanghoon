import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { BingoAppState } from 'modules';
import { actionCreators as gameActions } from 'modules/game';
import { GameStartButton } from 'components';


interface IProps {
  gameStatus: string;
  GameActions: typeof gameActions;
}

const mapStateToProps = (state: BingoAppState) => ({
  gameStatus: state.game.get('gameStatus'),
});

const mapDispatchToProps = (dispatch: any) => ({
  GameActions: bindActionCreators(gameActions, dispatch),
});

const GameStatus: React.FC<IProps> = (props) => {
  const { GameActions, gameStatus } = props;
  const startGame = () => {
    GameActions.refreshGame({});
  };

  return (
    <React.Fragment>
      <GameStartButton
        status={gameStatus}
        startGame={startGame}
      />
    </React.Fragment>
  );
};

export const GameStatusContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(GameStatus);
