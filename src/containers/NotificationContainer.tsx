import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { BingoAppState } from 'modules';
import { actionCreators as gameActions } from 'modules/game';
import { Maybe, NotificationPopup } from 'components';
import { Notification as NotificationType } from 'store';

interface IProps {
  notification: NotificationType | null;
  GameActions: typeof gameActions;
}

const mapStateToProps = (state: BingoAppState) => ({
  notification: state.game.get('notification'),
});

const mapDispatchToProps = (dispatch: any) => ({
  GameActions: bindActionCreators(gameActions, dispatch),
});

const Notification: React.FC<IProps> = (props) => {
  const { GameActions, notification } = props;

  const closeNotification = () => {
    GameActions.closeNotification({});
  };
  const initGame = () => {
    GameActions.initGame({});
  };


  return (
    <React.Fragment>
      <Maybe test={notification !== null}>
        <NotificationPopup
          data={notification}
          closeNotification={closeNotification}
          initGame={initGame}
        />
      </Maybe>
    </React.Fragment>
  );
};

export const NotificationContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Notification);
