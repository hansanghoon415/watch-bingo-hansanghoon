import * as React from 'react';
import { connect } from 'react-redux';

import { BingoAppState } from 'modules';
import { BingoContainer } from './BingoContainer';

interface IProps {
  bingoIds: string[];
}

const mapStateToProps = (state: BingoAppState) => ({
  bingoIds: state.game.bingos.getIn(['ids']),
});

const BingoList: React.FC<IProps> = (props) => {
  const { bingoIds } = props;

  return (
    <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
      {bingoIds.map(bingoId =>
        (<BingoContainer
          key={bingoId}
          id={bingoId}
        />))}
    </div>
  );
};

export const BingoListContainer = connect(
  mapStateToProps,
  null
)(BingoList);
