export interface Line {
  type: string;
  number: number;
  cellValues: number[];
}

export interface Cell {
  value: number;
  rowIndex: number;
  colIndex: number;
  checked: boolean;
}

export interface Notification {
  type: string;
  userIndexList?: number[];
}
