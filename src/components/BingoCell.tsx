import * as React from 'react';
import { Cell } from 'store';

interface BingoCellProps {
  data: Cell;
  gameStatus: string;
  checkNumber(value: number): void;
}

export const BingoCell: React.FC<BingoCellProps> = props => {
  const { gameStatus, data, checkNumber } = props;
  const { value, checked } = data;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const pressNumber = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    checkNumber(value);
  };

  return (
    <div
      className={checked ? 'checked-cell' : 'cell'}
      onClick={pressNumber}
    >
      {gameStatus === 'waiting' ? '' : value}
    </div>
  );
};
