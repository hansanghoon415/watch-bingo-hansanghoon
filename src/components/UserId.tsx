import * as React from 'react';

interface UserIdProps {
  userId: string;
}

export const UserId: React.FC<UserIdProps> = props => {
  const { userId } = props;

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100px',
      }}
    >
      <h2>{`${userId}P`}</h2>
    </div>
  );
};
