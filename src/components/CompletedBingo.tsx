import * as React from 'react';
import { Line } from 'store';

interface CompletedBingoProps {
  data: Line;
}

export const CompletedBingo: React.FC<CompletedBingoProps> = props => {
  const { data } = props;
  const { type, number, cellValues } = data;

  const format = cellValues.join(',');
  return (
    <div
    >
      {`${type}:${number} : values(${format})`}
    </div>
  );
};
