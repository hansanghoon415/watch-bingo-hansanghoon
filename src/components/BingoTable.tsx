import * as React from 'react';

import { BingoCell } from './BingoCell';
import { Cell } from 'store';

interface BingoTableProps {
  data: Cell[];
  gameStatus: string;
  tableSize: number;
  checkNumber(value: number): void;
}

export const BingoTable: React.FC<BingoTableProps> = props => {
  const { gameStatus, data, checkNumber, tableSize } = props;
  const sortedCells = data.sort((ele1, ele2) => {
    if (ele1.rowIndex > ele2.rowIndex || ele1.colIndex > ele2.colIndex) {
      return 1;
    }
    return -1;
  });


  return (
    <div
      className='table'
      style={{
        gridTemplateColumns: '1fr '.repeat(tableSize),
        gridTemplateRows: '1fr '.repeat(tableSize),
      }}
    >
      {sortedCells.map(cell =>
        (<BingoCell
          key={`${cell.colIndex},${cell.rowIndex}`}
          data={cell}
          checkNumber={checkNumber}
          gameStatus={gameStatus}
        />))}
    </div>
  );
};
