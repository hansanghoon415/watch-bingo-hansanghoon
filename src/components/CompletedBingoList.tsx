import * as React from 'react';

import { CompletedBingo } from './CompletedBingo';
import { Line } from 'store';

interface CompletedBingoListProps {
  data: Line[];
}

export const CompletedBingoList: React.FC<CompletedBingoListProps> = props => {
  const { data } = props;

  return (
    <div className='line-table'>
      {data.map(line =>
        (<CompletedBingo
          key={`${line.type}:${line.number}`}
          data={line}
        />))}
    </div>
  );
};
