import * as React from 'react';

interface GameStartButtonProps {
  status: string;
  startGame(): void;
}

export const GameStartButton: React.FC<GameStartButtonProps> = props => {
  const { status, startGame } = props;
  let text = '';
  switch (status) {
    case 'waiting': {
      text = '시작';
      break;
    }
    case 'playing': {
      text = '재시작';
      break;
    }
    default: {
      break;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const pressStart = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    startGame();
  };

  return (
    <div style={{ height: '100px', display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
      <div
        style={{ width: '150px', height: '65px', background: 'lightcoral' }}
        onClick={pressStart}
      >
        <h2>
          {text}
        </h2>
      </div>
    </div >
  );
};
