import * as React from 'react';

import { Notification } from 'store';

interface NotificationPopupProps {
  data: Notification | null;
  closeNotification(): void;
  initGame(): void;
}

export const NotificationPopup: React.FC<NotificationPopupProps> = props => {
  if (props.data === null) {
    return null;
  }
  const { closeNotification, initGame, data } = props;
  const { type, userIndexList } = data;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const closePopup = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    switch (type) {
      case 'not_your_turn': {
        closeNotification();
        break;
      }
      case 'win': {
        initGame();
        break;
      }
    }
  };
  let text = '';
  switch (type) {
    case 'not_your_turn': {
      text = '잘못된 차례입니다.';
      break;
    }
    case 'win': {
      if (!userIndexList) {
        return null;
      }
      if (userIndexList.length === 1) {
        text = `${userIndexList[0] + 1}P가 빙고를 완성했습니다.`;
      } else {
        text = '무승부입니다';
      }
      break;
    }
  }

  return (
    <div className={'popup'}>
      <div className={'popup-content'}>
        <h2>{text}</h2>
        <button onClick={closePopup}>
          확인
        </button>
      </div>
    </div >
  );
};
