import { createAction, handleActions, Action } from 'redux-actions';

import GameState from 'modules/game.state';
import { Bingo } from 'models';
import { getEnv } from 'functions/helper';

const USER_NUM = parseInt(getEnv('REACT_APP_USER_NUM'), 10);
const WIN_LINE = parseInt(getEnv('REACT_APP_WIN_LINE'), 10);

// *Actions
const INIT_GAME = 'bingo/game/INIT_GAME';
const REFRESH_GAME = 'bingo/game/REFRESH_GAME';
const CHECK_NUMBER = 'bingo/game/CHECK_NUMBER';
const CLOSE_NOTIFICATION = 'bingo/game/CLOSE_NOTIFICATION';
// *Actions payload type
type INIT_GAME_PAYLOAD = {};
type REFRESH_GAME_PAYLOAD = {};
type CHECK_NUMBER_PAYLOAD = { value: number; userId: number }
type CLOSE_NOTIFICATION_PAYLOAD = {}

// add field by middleware
type MIDDLEWARE_PAYLOAD = { now: number }
interface IAction<T> extends Action<T> {
  type: string;
  payload: T & MIDDLEWARE_PAYLOAD;
  error?: boolean;
  meta?: any;
}

// *Reducer
export default handleActions<GameState, any>({
  [INIT_GAME]: (state, action: IAction<INIT_GAME_PAYLOAD>) => {
    const { now } = action.payload;
    return state
      .set('gameStatus', 'playing')
      .set('nextUserId', 0)
      .set('notification', null)
      .set('gameStatus', 'waiting')
      .update('bingos', bingos => {
        const emptyBingos = bingos.empty();
        return state.userIds.reduce(
          (resultBingos, userId) => resultBingos.add(
            new Bingo(userId, now)
          ),
          emptyBingos
        );
      });
  },
  [REFRESH_GAME]: (state, action: IAction<REFRESH_GAME_PAYLOAD>) => {
    const { now } = action.payload;
    return state
      .set('gameStatus', 'playing')
      .set('nextUserId', 0)
      .set('notification', null)
      .update('bingos', bingos => {
        const emptyBingos = bingos.empty();
        return state.userIds.reduce(
          (resultBingos, userId) => resultBingos.add(
            new Bingo(userId, now)
          ),
          emptyBingos
        );
      });
  },

  [CHECK_NUMBER]: (state, action: IAction<CHECK_NUMBER_PAYLOAD>) => {
    const { value, userId } = action.payload;
    if (state.gameStatus === 'waiting') {
      return state;
    }
    if (state.nextUserId !== userId) {
      return state.set('notification', { type: 'not_your_turn' });
    }

    const bingoIds = state.bingos.get('ids');
    return state.withMutations((map: GameState) => {
      map.update('nextUserId',
        (nextUserId) => (nextUserId + 1) % state.userIds.length);
      bingoIds.forEach((bingoId) => {
        map.updateIn(['bingos', 'byId', bingoId],
          (ele: Bingo) => {
            const cellIndex = ele.cells.findIndex(cell => cell.value === value);
            if (cellIndex < 0) {
              return ele;
            }

            const newCompletedLines = ele.getNewCompletedLines(cellIndex);

            return ele
              .setIn(['cells', cellIndex, 'checked'], true)
              .update('completedLines', completedLines => completedLines.concat(newCompletedLines));
          }
        );
      });

      const winners: number[] = [];
      map.bingos.byId.forEach(bingo => {
        if (bingo.completedLines.length >= WIN_LINE) {
          winners.push(bingo.userId);
        }
      });
      if (winners.length !== 0) {
        map.set('notification', { type: 'win', userIndexList: winners });
      }
    });
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  [CLOSE_NOTIFICATION]: (state, action: IAction<CLOSE_NOTIFICATION_PAYLOAD>) => {
    return state.set('notification', null);
  },
}, new GameState(USER_NUM, Math.floor((new Date()).getTime())));

// *Action Creators
export const actionCreators = {
  initGame: createAction<INIT_GAME_PAYLOAD>(INIT_GAME),
  refreshGame: createAction<REFRESH_GAME_PAYLOAD>(REFRESH_GAME),
  checkNumber: createAction<CHECK_NUMBER_PAYLOAD>(CHECK_NUMBER),
  closeNotification: createAction<CLOSE_NOTIFICATION_PAYLOAD>(CLOSE_NOTIFICATION),
};
