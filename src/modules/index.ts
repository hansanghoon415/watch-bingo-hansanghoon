import { combineReducers } from 'redux';

import gameReducer from 'modules/game';
import GameState from 'modules/game.state';

const galconApp = combineReducers({
  game: gameReducer,
});

export default galconApp;

export type BingoAppState = {
  game: GameState;
}