import { Record } from 'immutable';
import { BingoList, Bingo } from 'models';
import { Notification } from 'store';

interface GameStateProp {
  bingos: BingoList;
  userIds: number[];
  nextUserId: number;
  gameStatus: string;
  notification: Notification | null;
}

const defaultGameStateProp: GameStateProp = {
  bingos: new BingoList([]),
  userIds: [],
  nextUserId: 0,
  gameStatus: '',
  notification: null,
};

export default class GameState extends Record(defaultGameStateProp, 'GameState') implements GameState {
  public constructor(userNum: number, now: number) {
    const userIds = [];
    for (let i = 0; i < userNum; i += 1) {
      userIds.push(i);
    }
    const bingos = userIds.reduce(
      (resultBingos, userId) => resultBingos.add(
        new Bingo(userId, now)
      ),
      new BingoList([])
    );
    super({ userIds, bingos, gameStatus: 'waiting' });
  }
}