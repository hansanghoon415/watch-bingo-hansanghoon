import { Record } from 'immutable';

import ObjectList from 'models/objectList';
import { Line, Cell } from 'store';
import uuidv4 from 'uuidv4';

import { getEnv } from 'functions/helper';

const TABLE_SIZE = parseInt(getEnv('REACT_APP_TABLE_SIZE'), 10);
const MAX_NUMBER = parseInt(getEnv('REACT_APP_MAX_NUMBER'), 10);

interface BingoProp {
  id: string;
  userId: number;
  tableSize: number;
  completedLines: Line[];
  cells: Cell[];
  createdAt: number;
}

const defaultBingoProp: BingoProp = {
  id: '',
  userId: 0,
  tableSize: 0,
  completedLines: [],
  cells: [],
  createdAt: 0,
};

export class Bingo extends Record(defaultBingoProp, 'Bingo') implements BingoProp {
  public constructor(userId: number, now: number) {
    const cells = [];
    let numbers: number[] = [];
    for (let i = 0; i < MAX_NUMBER; i++) {
      numbers.push(i + 1);
    }
    for (let rowIndex = 0; rowIndex < TABLE_SIZE; rowIndex += 1) {
      for (let colIndex = 0; colIndex < TABLE_SIZE; colIndex += 1) {
        const randomIndex: number = Math.floor((Math.random() * numbers.length)) % numbers.length;
        cells.push({
          value: numbers[randomIndex],
          rowIndex,
          colIndex,
          checked: false,
        });
        numbers = numbers.filter(ele => ele !== numbers[randomIndex]);
      }
    }
    const init = {
      id: uuidv4(),
      userId,
      tableSize: TABLE_SIZE,
      completedLines: [],
      cells,
      createdAt: now,
    };
    super(init);
  }

  public getNewCompletedLines(cellIndex: number) {
    const cell: Cell = this.getIn(['cells', cellIndex]);
    cell.checked = true;

    const newCompletedLines: Line[] = [];

    const rowCellList = this.getRowCellList(cell.rowIndex);
    const colCellList = this.getColCellList(cell.colIndex);
    if (rowCellList.every(ele => ele.checked === true)) {
      newCompletedLines.push({
        type: 'row',
        number: cell.rowIndex,
        cellValues: rowCellList.map(cell => cell.value),
      });
    }
    if (colCellList.every(ele => ele.checked === true)) {
      newCompletedLines.push({
        type: 'col',
        number: cell.colIndex,
        cellValues: colCellList.map(cell => cell.value),
      });
    }
    if (cell.rowIndex === cell.colIndex) {
      const digonalList1 = this.getDigonalList1();
      if (digonalList1.every(ele => ele.checked === true)) {
        newCompletedLines.push({
          type: 'digonal',
          number: 1,
          cellValues: digonalList1.map(cell => cell.value),
        });
      }
    }
    if (cell.rowIndex + cell.colIndex === this.tableSize - 1) {
      const digonalList2 = this.getDigonalList2();
      if (digonalList2.every(ele => ele.checked === true)) {
        newCompletedLines.push({
          type: 'digonal',
          number: 2,
          cellValues: digonalList2.map(cell => cell.value),
        });
      }
    }

    return newCompletedLines;
  }

  private getRowCellList(index: number) {
    return this.cells.filter(ele => ele.rowIndex === index);
  }

  private getColCellList(index: number) {
    return this.cells.filter(ele => ele.colIndex === index);
  }

  private getDigonalList1() {
    return this.cells.filter(ele => ele.colIndex === ele.rowIndex);
  }

  private getDigonalList2() {
    return this.cells.filter(ele => this.tableSize - 1 === ele.colIndex + ele.rowIndex);
  }
}

export class BingoList extends ObjectList(Bingo) { }
