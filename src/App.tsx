import React from 'react';
import './App.css';
import { BingoListContainer, GameStatusContainer, NotificationContainer } from 'containers';

const App: React.FC = () => {
  return (
    <div className='App'>
      <GameStatusContainer />
      <BingoListContainer />
      <NotificationContainer />
    </div>
  );
};

export default App;
