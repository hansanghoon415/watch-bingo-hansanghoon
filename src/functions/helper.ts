const getEnv = (name: string): string => {
  if (!name || !process.env[name]) {
    console.log(`process.env.${name} NOT EXIST`);
    return '';
  }
  return process.env[name] || '';
};

export {
  getEnv,
};
